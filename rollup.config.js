//@ts-check

const minify = require("@rollup/plugin-terser").default;

const globals = {
  "https://esm.sh/micromark@3?bundle": "micromark",
  "https://esm.sh/micromark-extension-frontmatter@1?bundle":
    "micromark-extension-frontmatter",
  "https://esm.sh/yaml@2?bundle": "yaml",
};

const external = Object.keys(globals);

const allFiles = require("fs")
  .readdirSync("src")
  .filter((file) => require("path").extname(file) === ".mjs")
  .map((file) => `src/${file}`);

const defaultConfig = {
  name: "RadUtils",
  dir: "dist",
  sourcemap: true,
  globals,
};

const singleEntryOutput = {
  format: "umd",
  amd: {
    id: "rad-utils",
  },
  ...defaultConfig,
  dir: undefined,
};

module.exports = [
  {
    input: "src/index.mjs",
    external,
    output: {
      file: "dist/main.js",
      ...singleEntryOutput,
    },
  },
  {
    input: "src/index.mjs",
    external,
    output: {
      file: "dist/main.min.js",
      ...singleEntryOutput,
      plugins: [minify()],
    },
  },
  {
    input: allFiles,
    external,
    output: {
      entryFileNames: "[name].js",
      format: "cjs",
      ...defaultConfig,
    },
  },
  {
    input: allFiles,
    external,
    output: {
      entryFileNames: "[name].min.js",
      format: "cjs",
      plugins: [minify()],
      ...defaultConfig,
    },
  },
  {
    input: allFiles,
    external,
    output: {
      entryFileNames: "[name].mjs",
      format: "es",
      ...defaultConfig,
    },
  },
];
