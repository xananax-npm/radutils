//@ts-check
import {isElement} from "./isElement.mjs";

/**
 * Little utility so people can pass css selectors or elements in initialization
 * options.
 * A minimal replacement to a more full fledged selector engine like jQuery
 * @param {string|HTMLElement} elementOrString
 * @returns {HTMLElement | null}
 */
export const getElement = (elementOrString) => {
  const element =
    elementOrString && typeof elementOrString === "string"
      ? /** @type {HTMLElement}*/ (document.querySelector(elementOrString))
      : elementOrString;
  return isElement(element) ? element : null;
};

export default getElement