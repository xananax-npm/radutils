//@ts-check

/**
 * Retries a promise N times, allowing it to fail by silently swallowing
 * errors, until `N` has run out.
 * @template {any} T
 * @param {()=>Promise<T>} promiseProviderFunc
 * @param {number} [max]
 * @returns {Promise<T>}
 */
export const retryPromise = (promiseProviderFunc, max = 5) => {
  if(max <= 0){
    return promiseProviderFunc()
  }
  /** @type {Promise<T>} */
  let promise = Promise.reject();

  for (let i = 0; i < max; i++) {
    promise = promise.catch(promiseProviderFunc);
  }
  return promise;
};

export default retryPromise