//@ts-check

/**
 * @template {any} T
 * @typedef {{
 *	resolve: (value: T) => void
 *	reject: (reason?: any) => void
 * } & Promise<T>} DeferredPromise<T> A promise that can be resolved externally
 */

/**
 * Returns a promise that can be resolved externally.
 * @template {any} DeferredPromiseType
 * @returns {DeferredPromise<DeferredPromiseType>}
 */
export const deferredPromise = () => {
  /** @type {(value: DeferredPromiseType) => void} */
	let resolve
  /** @type {(reason?: any) => void} */
  let reject;

  /**
   * @type {Promise<DeferredPromiseType>}
   */
	const promise = new Promise((_resolve, _reject) => {
		resolve = _resolve;
		reject = _reject;
	});

	// @ts-ignore
	return Object.assign(promise, {resolve, reject});
}

export default deferredPromise