//@ts-check
import {decodeContentLength} from './decodeContentLength.mjs'
import {fetchHeaders} from './fetchHeaders.mjs'

/**
 * Attempts to retrieve the size of an object represented by a URL with a
 * limited fetch request.
 * @see {decodeContentLength}
 * @param {string} path
 */
export const fetchContentLength = (path) =>
  fetchHeaders(path).then(decodeContentLength);

export default fetchContentLength