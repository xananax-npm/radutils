//@ts-check

/**
 * Limited fetch request that retrieves only headers.
 * @param {string} path
 */
export const fetchHeaders = async (path) => {
  const response = await fetch(path, {
    method: "HEAD",
    headers: {
      Range: "bytes=0-0",
      "X-HTTP-Method-Override": "HEAD",
    },
  });

  if (!response.ok) {
    throw new Error(`Failed loading file '${path}'`);
  }
  return response.headers;
};

export default fetchHeaders;
