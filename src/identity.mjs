/**
 * @template T
 * @param {T} [value]
 * @returns {Promise<T>}
 */
export const awaitedIdentity = (value) => Promise.resolve(value);

/**
 * @template T
 * @param {T} [value]
 * @returns {T}
 */
export const identity = (value) => value

identity.awaited = awaitedIdentity;

export default identity;