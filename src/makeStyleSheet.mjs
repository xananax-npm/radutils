//@ts-check


/**
 * Returns a stylesheet
 * @param {string} css_string 
 * @returns 
 */
export const makeStyleSheet = (css_string) => {
  const stylesheet = new CSSStyleSheet();
  stylesheet.replaceSync(css_string);
  return stylesheet
}

/**
 * Convenience literal to create DOM CSSStyleSheet instances
 * @param {TemplateStringsArray} strings 
 * @param  {...any} substitutions 
 */
export const css = (strings, ...substitutions) => {
  const formattedString = strings.reduce((acc, curr, index) => acc + curr + (substitutions[index]||''), '');
  console.log(formattedString)
  return makeStyleSheet(formattedString);
}