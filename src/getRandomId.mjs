//@ts-check

/**
 * short random string for ids - not guaranteed to be unique
 * @see https://www.codemzy.com/blog/random-unique-id-javascript
 * @param {number} length the length of the id
 */
export const getRandomId = function (length = 6) {
  return Math.random()
    .toString(36)
    .substring(2, length + 2);
};

export default getRandomId