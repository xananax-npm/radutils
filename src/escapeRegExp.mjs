//@ts-check

/**
 * Escapes a string so it can be used in a regular expression
 * @param {string} text
 */
export function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

export default escapeRegExp