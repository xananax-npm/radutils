//@ts-check

/**
 * @template T
 * @param {T} value
 * @returns {value is NonNullable<T>}
 */
export const isNotNull = (value) => value !== null;

export default isNotNull;
