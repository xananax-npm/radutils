//@ts-check

/**
 * A small utility to query elements and get back an array
 * @template {keyof HTMLElementTagNameMap} K
 * @type {{
 * (selector: K): HTMLElementTagNameMap[K][]
 * (parent: ParentNode, selector: K): HTMLElementTagNameMap[K][]
 * }}
 */
// @ts-ignore
export const querySelectorAll = (parent, selector) =>
  // @ts-ignore
  typeof selector === "undefined"
    ? // @ts-ignore
      querySelectorDoc(/** @type {keyof HTMLElementTagNameMap} */ (parent))
    : querySelectorParent(parent, selector);

/**
 * A small utility to query elements in the document and get back an array
 * @template {keyof HTMLElementTagNameMap} K
 * @param {K} selector
 * @returns {HTMLElementTagNameMap[K][]}
 */
export const querySelectorDoc = (selector) => [
  ...document.querySelectorAll(selector),
];

/**
 * A small utility to query elements in a parent and get back an array
 * @template {keyof HTMLElementTagNameMap} K
 * @param {ParentNode} parent
 * @param {K} selector
 * @returns {HTMLElementTagNameMap[K][]}
 */
export const querySelectorParent = (parent, selector) => [
  ...parent.querySelectorAll(selector),
];

export default querySelectorAll;
