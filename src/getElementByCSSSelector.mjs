//@ts-check
import { isLocalHost } from "./isLocalHost.mjs";

/**
 * Gets an element by a valid selector if the element exists, otherwise throws, but only if running in localhost environments.
 * Use this in the initial setup to verify all elements exist
 * @param {string} selector
 * @return {HTMLElement}
 */
export const getElementByCSSSelector = (selector) => {
  const element = document && document.querySelector(selector);
  if (isLocalHost && !element) {
    throw new Error(`Element "#${selector}" was not found`);
  }
  // @ts-ignore
  return element;
};

export default getElementByCSSSelector