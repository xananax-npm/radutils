//@ts-check

/**
 * Changes the document's title. Instead of passing the main title, you can also
 * change the functions `title` member:
 * ```js
 * changeTitle.title = "My Site"
 * changeTitle("Home") // produces "Home | My Site"
 * ```
 * if not `title` is passed, the document title (as found when running the function
 * the first time) will be used.
 * @param {string} title
 * @param {string} mainTitle
 */
export const changeTitle = (title, mainTitle = _changeTitle.title) =>
  document && (document.title = `${title} | ${mainTitle}`);

const _changeTitle = Object.assign(changeTitle, {
  title: (document && document.title) || ""
})

export default _changeTitle;
