//@ts-check

import { changeTitle } from "./changeTitle.mjs";
//import { createCustomElement } from "./createCustomElement.mjs";
import { createElementStatusModes } from "./createElementStatusModes.mjs";
import { createTrackedResponse } from "./createTrackedResponse.mjs";
import { decodeContentLength } from "./decodeContentLength.mjs";
import { deferredPromise } from "./deferredPromise.mjs";
import { documentState } from "./documentState.mjs";
import { elementMode } from "./elementMode.mjs";
import { escapeRegExp } from "./escapeRegExp.mjs";
import { fetchContentLength } from "./fetchContentLength.mjs";
import { fetchHeaders } from "./fetchHeaders.mjs";
import { fetchMarkdown } from "./fetchMarkdown.mjs";
import { fetchText } from "./fetchText.mjs";
import { generateDomFromString } from "./generateDomFromString.mjs";
import { getAspectRatio } from "./getAspectRatio.mjs";
import {
  getCurrentHashUrl,
  hasCurrentHashUrl,
  hasNoHashUrl,
} from "./getCurrentHashUrl.mjs";
import { getElement } from "./getElement.mjs";
import { getElementByCSSSelector } from "./getElementByCSSSelector.mjs";
import { getElementById } from "./getElementById.mjs";
import { getFirstTitleContent } from "./getFirstTitleContent.mjs";
import { getLocale } from "./getLocale.mjs";
import { getPageUniqueId } from "./getPageUniqueId.mjs";
import { getRandomId } from "./getRandomId.mjs";
import { getReasonableUuid } from "./getReasonableUuid.mjs";
import { identity, awaitedIdentity } from "./identity.mjs";
import { isElement } from "./isElement.mjs";
import { html } from "./html.mjs";
import { isExternalUrl } from "./isExternalUrl.mjs";
import { isLocalHost } from "./isLocalHost.mjs";
import { isNotNull } from "./isNotNull.mjs";
import { makeBoundConsole } from "./makeBoundConsole.mjs";
import { makeEventEmitter } from "./makeEventEmitter.mjs";
import { makeFileLoader, makeFileLoadersTracker } from "./makeFileLoader.mjs";
import { makeFileSizeFetcher } from "./makeFileSizeFetcher.mjs";
import { makeSignal } from "./makeSignal.mjs";
import { makeStyleSheet, css } from "./makeStyleSheet.mjs";
import { makeTemplate, tmpl } from "./makeTemplate.mjs";
import { markdownToMarkup } from "./markdownToMarkup.mjs";
import { markupToDom } from "./markupToDom.mjs";
import { memoize } from "./memoize.mjs";
import { noOp } from "./noOp.mjs";
import { not } from "./not.mjs";
import {
  onDocumentKeyUp,
  onDocumentKeyDown,
  onDocumentKey,
} from "./onDocumentKey.mjs";
import {
  basename,
  filename,
  stripExtension,
  dirName,
  extension,
  metadata,
} from "./path.mjs";
import { percentFromProgress } from "./percentFromProgress.mjs";
import { print, makeMiniStringTemplate } from "./print.mjs";
import {
  querySelectorDoc,
  querySelectorParent,
  querySelectorAll,
} from "./querySelectorAll.mjs";
import { retryPromise } from "./retryPromise.mjs";
import { rewriteLocalUrls } from "./rewriteLocalUrls.mjs";
import { throttle } from "./throttle.mjs";
import { today } from "./today.mjs";
import { trackProgressWithCSS } from "./trackProgressWithCSS.mjs";
import { UnreachableCaseError } from "./UnreachableCaseError.mjs";
import { wait } from "./wait.mjs";
import { waitIfLocalHost } from "./waitIfLocalHost.mjs";

export {
  changeTitle,
  createElementStatusModes,
  createTrackedResponse,
  decodeContentLength,
  deferredPromise,
  elementMode as documentMode,
  documentState,
  escapeRegExp,
  fetchContentLength,
  fetchHeaders,
  fetchMarkdown,
  fetchText,
  generateDomFromString,
  getAspectRatio,
  getCurrentHashUrl,
  hasCurrentHashUrl,
  hasNoHashUrl,
  getElement,
  getElementByCSSSelector,
  getElementById,
  getFirstTitleContent,
  getLocale,
  getPageUniqueId,
  getRandomId,
  getReasonableUuid,
  html,
  identity,
  awaitedIdentity,
  isElement,
  isExternalUrl,
  isLocalHost,
  isNotNull,
  makeBoundConsole,
  makeEventEmitter,
  makeFileLoader,
  makeFileLoadersTracker,
  makeFileSizeFetcher,
  makeSignal,
  makeStyleSheet, 
  css,
  makeTemplate, 
  tmpl,
  markdownToMarkup,
  markupToDom,
  memoize,
  not,
  noOp,
  onDocumentKeyUp,
  onDocumentKeyDown,
  onDocumentKey,
  basename,
  filename,
  stripExtension,
  dirName,
  extension,
  metadata,
  percentFromProgress,
  print,
  makeMiniStringTemplate,
  querySelectorDoc,
  querySelectorParent,
  querySelectorAll,
  retryPromise,
  rewriteLocalUrls,
  throttle,
  today,
  trackProgressWithCSS,
  UnreachableCaseError,
  wait,
  waitIfLocalHost,
};
