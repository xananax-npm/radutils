//@ts-check

/**
 * Retrieves the current locale. Only works if `navigator` is available.
 * Otherwise, returns the `defaultLang` passed property
 * @param {string} [defaultLang] defaults to an empty string
 * @returns The browser locale, formatted as `xx_XX`
 */
export const getLocale = (defaultLang = "") =>
  (typeof navigator !== "undefined" &&
    (navigator.languages ? navigator.languages[0] : navigator.language)
      .split(".")[0]
      .replace("-", "_")) ||
  defaultLang;

export default getLocale;
