//@ts-check

/**
 * Does absolutely nothing. Use it when you need a function that does nothing
 * at all.
 */
export const noOp = () => {}
export default noOp