//@ts-check

/**
 * Waits the specified amount of time before returning the value
 * @param {number} durationMs Duration, in milliseconds. Defaults to 1 second
 * @returns
 */
export const wait =
  (durationMs = 1000) =>
  /**
   * @template T
   * @param {T} [value]
   * @returns {Promise<T>}
   */
  (value) =>
    new Promise((ok) => setTimeout(ok, durationMs, value));

export default wait;
