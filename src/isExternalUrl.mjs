//@ts-check

/**
 * Assumes a provided url is external if it begins by a known protocol
 * @param {string} url
 */
export const isExternalUrl = (url) =>
  url && /^(https?|mailto|tel|ftp|ipfs|dat):/.test(url);

export default isExternalUrl;
