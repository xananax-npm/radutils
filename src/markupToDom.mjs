//@ts-check
import { generateDomFromString } from "./generateDomFromString.mjs";
import { getFirstTitleContent } from "./getFirstTitleContent.mjs";
import { markdownToMarkup } from './markdownToMarkup.mjs';


/**
 * Takes a markdown string and transforms it into dom that can be slotted in a
 * document.
 * Additionally, it parses the frontmatter, and attempts to extract a title
 * by finding either the first title in the doc, or the filename (if provided).
 * @param {string} markdownStr 
 * @param {string} [path] the file path
 */
export const markupToDom = (markdownStr, path = '') => {
  const { frontMatter, markup } = markdownToMarkup(markdownStr);
  const content = generateDomFromString(markup);
  const title = getFirstTitleContent(content) || path.replace(/\.\w{2, 4}$/, "");
  return { title, raw: markdownStr, content, frontMatter };
};
