//@ts-check

import isExternalUrl from "./isExternalUrl.mjs";
import { querySelectorParent } from "./querySelectorAll.mjs";

/**
 * Makes sure urls to local pages are prepended with #/
 * @param {ParentNode} container the element containing links to find
 */
export const rewriteLocalUrls = (container) => {
  querySelectorParent(container, "a").forEach((a) => {
    const href = a.getAttribute("href");
    if (href && !isExternalUrl(href) && !href.startsWith("#")) {
      a.setAttribute("href", "#/" + href.replace(/^\.?\//, ""));
    }
  });
  return container;
};

export default rewriteLocalUrls;
