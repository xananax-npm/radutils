//@ts-check

//@ts-ignore
import { micromark } from "https://esm.sh/micromark@3?bundle";
//@ts-ignore
import {frontmatter, frontmatterHtml} from 'https://esm.sh/micromark-extension-frontmatter@1?bundle'
//@ts-ignore
import Yaml from 'https://esm.sh/yaml@2?bundle'

/**
 * Transforms a markup string into a valid markup.
 * If there's a YAML frontmatter, will parse it too
 * @param {string} markdownStr 
 * @returns 
 */
export const markdownToMarkup = (markdownStr) => {
  /** @type {string} */
  const markup = micromark(markdownStr, {
    extensions: [frontmatter()],
    htmlExtensions: [frontmatterHtml()]
  })
  const header = Yaml.parseAllDocuments(markdownStr)[0]
  /** @type {Record<string, unknown>} */
  const frontMatter = header ? header.toJS() : {}
  return { markup, frontMatter }
}

export default markdownToMarkup