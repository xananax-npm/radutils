/**
 * Mini-mustache templating system. Simply replaces all occurrences of {{key}} with the value of the key. 
 * @param {string} str 
 * @param {Record<string, any>} replacements 
 */
export const print = (str, replacements) => 
  str.replace(/{{(.*?)}}/g, (_match, key) => replacements[key] || '')


/**
 * 
 * @param {string} str 
 * @returns {(replacements: Record<string, any>) => string}
 */
export const makeMiniStringTemplate = (str) => print.bind(null, str)

print.makeTemplate = makeMiniStringTemplate

export default print