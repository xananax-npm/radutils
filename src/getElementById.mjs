//@ts-check
import { isLocalHost } from "./isLocalHost.mjs";

/**
 * Gets an element by id if the element exists, otherwise throws, but only if running in localhost environments.
 * Use this in the initial setup to verify all elements exist
 * @param {string} id
 * @return {HTMLElement}
 */
export const getElementById = (id) => {
  const element = document && document.getElementById(id);
  if (isLocalHost && !element) {
    throw new Error(`Element "#${id}" was not found`);
  }
  // @ts-ignore
  return element;
};

export default getElementById;
