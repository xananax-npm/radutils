//@ts-check

/** @type {boolean} returns true if the window global object exists and the domain is localhost of 127.0.0.1 */
export const isLocalHost =
  typeof window !== "undefined" &&
  /^localhost|127.0.0.1/.test(window.location.hostname);

export default isLocalHost;
