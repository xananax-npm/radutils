//@ts-check

const methods = /** @type {('warn' & keyof typeof console)[]} */(["log", "warn", "error"])

/**
 * Returns console methods that can be used standalone (without requiring `console`).
 * Optional prefix will prepend every call with the provided prefix
 * @param {any} prefix
 */
export const makeBoundConsole = (prefix = "") => {
  const [log, warn, error] = methods.map(
    (fn) =>
      (/** @type {any} */ msg) =>
        console[fn](prefix, msg)
  );
  return { log, warn, error };
};

export default makeBoundConsole