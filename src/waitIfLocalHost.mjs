//@ts-check
import wait from "./wait.mjs";
import { awaitedIdentity } from "./identity.mjs";
import isLocalHost from "./isLocalHost.mjs";


/**
 * Waits the specified amount of time before returning the value
 * @param {number} _durationMs Duration, in milliseconds. Defaults to 1 second
 * @returns
 */
const fakeWait = (_durationMs = 1000) => awaitedIdentity;


/**
 * useful to check for transitions while developing styles, if the loading screen
 * disappears too fast for example.
 * @template T
 * @param {number} durationMs Duration, in milliseconds. Defaults to 1 second
 * @returns {(value?: T | undefined) => Promise<T>}
 */
export const waitIfLocalHost = isLocalHost ? wait : fakeWait;

export default waitIfLocalHost;
