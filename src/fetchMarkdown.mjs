//@ts-check

import { fetchText } from "./fetchText.mjs";
import { waitIfLocalHost } from "./waitIfLocalHost.mjs";
import { markupToDom } from "./markupToDom.mjs"

/**
 * Loads and parses a markdown document. Makes use of micromark.
 * @param {string} path the path to load
 */
export const fetchMarkdown = (path) =>
  fetchText(path)
    .then(waitIfLocalHost())
    .then(raw => markupToDom(raw, path));

export default fetchMarkdown;
