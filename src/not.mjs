/**
 * Inverter.
 * Easier to read than "!"
 * @param {any} a 
 * @returns 
 */
export const not = a => !a

export default not