//@ts-check

/**
 * A reasonably unique id.
 * Deterministic, it will always return the same id for the same call order
 */
export const getReasonableUuid = (() => {
  let start = 100000000;
  return () => (start++).toString(36);
})();

export default getReasonableUuid