/**
 * Does exactly the same as simply using ``, but allows to
 * neatly highlight the html string in editors that support it.
 * Packages the resulting string as a template
 * @param {TemplateStringsArray} strings 
 * @param  {...any} expressions 
 */
export function html(strings, ...expressions){
  const parsed = strings.reduce((previous, current, i) => {
      return previous + current + (expressions[i] ? expressions[i] : '')
  }, '')
  const template = document.createElement("template")
  template.innerHTML = parsed
  return template
}

export default html