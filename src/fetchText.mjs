//@ts-check

import isLocalHost from "./isLocalHost.mjs";

/**
 * Loads a text file. The path provided will be appened with a random number
 * when running on localhost to avoid caching problems
 * @param {string} path
 * @returns {Promise<string>} the loaded file
 */
export const fetchText = (path) =>
  fetch(isLocalHost ? `./${path}?rand=${Math.random()}` : `./${path}`).then(
    (response) => response.text()
  );

export default fetchText;
