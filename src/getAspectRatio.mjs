//@ts-check

/**
 * @typedef {{width: number;height: number}} Size
 */

/**
 * Calculates an ideal ratio
 * @param {Size} initial the initial size
 * @param {Size} current the current size
 * @returns
 */
export const getAspectRatio = (initial, current) => {
  const ratioW = current.width / initial.width;
  const ratioH = current.height / initial.height;
  const ratio = Math.min(ratioW, ratioH);
  const width = initial.width * ratio;
  const height = initial.height * ratio;
  return { width, height, ratio };
};

export default getAspectRatio