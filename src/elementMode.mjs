//@ts-check

/**
 * Creates a helper to add or remove global classes that begin with `is-`
 * @param {string} name name of the state
 * @param {Element} [element] defaults to the document body
 */
export const elementMode = (name, element = document.body) => ({
  on: () => element.classList.add(`is-${name}`),
  off: () => element.classList.remove(`is-${name}`),
  toggle: () => element.classList.toggle(`is-${name}`),
  has: () => element.classList.contains(`is-${name}`),
});

export default elementMode;
