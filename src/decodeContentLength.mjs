//@ts-check

/**
 * Does a best guess attempt at finding out the size of a file from a request headers.
 * To access headers, server must send CORS header
 * `Access-Control-Expose-Headers: content-encoding, content-length x-file-size`
 * server must send the custom `x-file-size` header if gzip or other content-encoding is used.
 * @param {Headers} headers
 */
export const decodeContentLength = (headers) => {
  const contentEncoding = headers.get("content-encoding");
  const contentLength =
    headers.get(contentEncoding ? "x-file-size" : "content-length") ||
    (headers.has("content-range") &&
      /**@type {string}*/ (headers.get("content-range")).split("/")[1]) ||
    "0";
  return parseInt(contentLength, 10);
};

export default decodeContentLength;
