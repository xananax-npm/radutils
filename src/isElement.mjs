//@ts-check

/**
 * Verifies an element is actually an element.
 * @param {any} element
 * @returns {element is HTMLElement}
 */
export const isElement = (element) => {
  return element instanceof Element || element instanceof Document;
};

export default isElement