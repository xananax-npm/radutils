/**
 * Returns the first title content in the document, if there is one.
 * @param {ParentNode} content
 * @returns
 */
export const getFirstTitleContent = (content = document) => {
  /** @type {HTMLHeadElement} */
  const firstTitleElement = content.querySelector("h1");
  return firstTitleElement ? firstTitleElement.textContent || "" : "";
};

export default getFirstTitleContent;
