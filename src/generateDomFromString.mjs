//@ts-check

/**
 * Generates valid dom elements from a string
 * @param {string} htmlString
 */
export const generateDomFromString = (htmlString) => {
  
  htmlString = htmlString.trim();
  const dom = new DOMParser().parseFromString('<template>'+ htmlString +'</template>','text/html')
  const content = /** @type {HTMLTemplateElement} */(dom.head.firstElementChild).content

  const fragment = document.createDocumentFragment();
  fragment.append(content);

  return fragment;
};

export default generateDomFromString;

/**
 * @param {String} html representing a single element
 * @return {Element|null}
 */
function htmlToElement(html) {
  html = html.trim();
  var template = document.createElement("template");
  template.innerHTML = html;
  if (template.content.childNodes.length) {
  }
  return /** @type {Element} */ (template.content.firstChild);
}

/**
 * @param {String} html representing any number of sibling elements
 * @return {NodeList}
 */
function htmlToElements(html) {
  var template = document.createElement("template");
  template.innerHTML = html;
  return template.content.childNodes;
}
