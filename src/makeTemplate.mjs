//@ts-check


/**
 * Returns a template
 * @param {string} template_string 
 * @returns 
 */
export const makeTemplate = (template_string) => {
  const template = document.createElement("template")
  template.innerHTML = template_string
  return template
}

/**
 * Convenience literal to create DOM template elements
 * @param {TemplateStringsArray} strings 
 * @param  {...any} substitutions 
 */
export const tmpl = (strings, ...substitutions) => {
  const formattedString = strings.reduce((acc, curr, index) => acc + curr + (substitutions[index]||''), '');
  return makeTemplate(formattedString);
}