
/**
 * 
 * @param {string} str 
 * @param {string} [sep] defaults to `/`
 */
export const filename = (str, sep = '/') => str.slice(str.lastIndexOf(sep) + 1)

/**
 * @param {string} str 
 */
export const stripExtension = (str) => str.slice(0,str.lastIndexOf('.'))

/**
 * 
 * @param {string} str 
 * @param {string} [sep] defaults to `/`
 */
export const basename = (str, sep = '/') => stripExtension(filename(str, sep))

/**
 * @param {string} str 
 */
export const extension = (str) => str.slice(str.lastIndexOf('.') + 1)

/**
 * @param {string} str 
 * @param {string} [sep] defaults to `/`
 */
export const dirName = (str, sep = '/') => str.slice(0, str.lastIndexOf(sep) + 1)

export const metadata = (str, sep = '/') => ({
  basename: basename(str, sep),
  filename: filename(str, sep),
  extension: extension(str),
  dirName: dirName(str),
  fullPath: str
})

export default { basename, filename, stripExtension, dirName, extension, metadata }