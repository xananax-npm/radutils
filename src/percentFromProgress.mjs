//@ts-check

/**
 * Returns a formatted percent from a given fraction.
 * @param {number} fraction any fractional number, e.g, 5/10
 * @param {boolean} [pad]
 */
export const percentFromProgress = (fraction, pad = false) =>
  /** @type {`${string}%`} */ (Math.round(fraction * 100) + "%").padStart(pad? 3 : 0, "0");

export default percentFromProgress