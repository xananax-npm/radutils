# RADUtils

A bunch of small, hopefully readable modules that ease development time.

## Goals:

- No build step
- No dependencies
- ES6 modules (assume modern JS environments)
- Fully typed (_no_ typescript, entirely JSDoc based)
- Minimal and readable
- Not customizable: the API is the code. To modify, copy-paste in your own project.


## Intended Use

The intended use for this library is either:

1. Load sub-modules directly from a CDN (such as https://esm.sh/) to create applications directly in an html page.
2. prototype quickly, then later rip out the utilities and paste them in your own project once your API solidifies.